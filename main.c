#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "voAAC.h"
#include "cmnMemory.h"
#include "assert.h"

#define READ_SIZE   (1024 * 8)

static int ReadFile2Buf(
    FILE           *infile,
    unsigned char  *dest,
    int             readSize
    )
{
    int readBytes = 0;


    assert(dest != NULL); 
    assert(infile != NULL); 

    readBytes = fread(dest, 1, readSize, infile);

    return readBytes;
}


int main(
    int     argc, 
    char  **argv
    )
{
    VO_MEM_OPERATOR         moper;
    VO_CODEC_INIT_USERDATA  useData;
    unsigned char           inBuffer[READ_SIZE]; 
    unsigned char           outBuffer[READ_SIZE]; 
    VO_CODECBUFFER          inData;
    VO_CODECBUFFER          outData;
    VO_HANDLE               hCodec;
    VO_AUDIO_OUTPUTINFO     outInfo;
    VO_AUDIO_CODECAPI       AudioAPI;
    FILE                   *inFile; 
    FILE                   *outFile; 
    AACENC_PARAM            aacpara;
    char                    eof = 0; 


    inFile = fopen("in.wav", "rb"); 
    assert(inFile != NULL); 

    outFile = fopen("out.aac", "wb"); 
    assert(outFile != NULL); 

    // set memory operators;
    moper.Alloc = cmnMemAlloc;
    moper.Copy = cmnMemCopy;
    moper.Free = cmnMemFree;
    moper.Set = cmnMemSet;
    moper.Check = cmnMemCheck;
    useData.memflag = VO_IMF_USERMEMOPERATOR;
    useData.memData = (VO_PTR)(&moper);

    voGetAACEncAPI(&AudioAPI); 

    // Initialize. 
    int rv = AudioAPI.Init(&hCodec, VO_AUDIO_CodingAAC, &useData);
    assert(rv == 0); 

    // Set parameter. 
    aacpara.adtsUsed = 1; 
    aacpara.nChannels = 2; 
    aacpara.sampleRate = 44100; 
    aacpara.bitRate = 128000; 
    
    // Set parameter. 
    rv = AudioAPI.SetParam(hCodec, VO_PID_AAC_ENCPARAM, &aacpara);
    assert(rv == 0); 

    do
    {
        // Assign input buffer. 
        inData.Buffer = inBuffer;
        inData.Length = fread(inData.Buffer, 1, READ_SIZE, inFile); 

        printf("read bytes = %d\n", inData.Length); 

        // Set input data. 
        rv = AudioAPI.SetInputData(hCodec, &inData); 
        assert(rv == 0); 

        // An input data may result in multiple frames of output, thus we 
        // need to do while and fetch all frames. 
        do 
        {
            // Set output data. 
            outData.Buffer = outBuffer; 
            outData.Length = READ_SIZE; 

            // Get output data. 
            rv = AudioAPI.GetOutputData(hCodec, &outData, &outInfo); 
            if(rv == 0)
            {
                fwrite(outData.Buffer, 1, outData.Length, outFile); 
                printf("write bytes = %d\n", outData.Length); 
            }
            else if(rv == VO_ERR_INPUT_BUFFER_SMALL)
            {
                break; 
            }
            else
            {
                printf("AudioAPI.GetOutputData() error = 0x%x\n", rv); 
                assert(0); 
            }
            
        } while(1); 

    } while(!feof(inFile)); 

    fclose(inFile);
    fclose(outFile);
}
